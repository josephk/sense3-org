+++

date = "2018-02-28T14:41:00+01:00"
title = "Accueil"
author = "JosephK"
draft = false
type = "page"
id = "-home"
+++

{{% grid class="row intro" %}}
{{% grid class="container" %}}
{{% grid class="clearfix" %}}
{{% grid class="col-sm-12 text-center" %}}

# « Ceci n’est pas une pub »

## Faisons de la réclame pour des projets qui ont du sens

{{% /grid %}}
{{% /grid %}}
{{% grid class="clearfix" %}}
{{% grid class="col-md-6 text-center" %}}
<div class="web-browser">
    <div class="toolbar">
        <img src="/browser-left.svg" alt="" />
        <div class="search-bar"></div>
        <img src="/browser-right.svg" alt="" />
    </div>
    <img src="/wireframe.png" class="img-responsive" alt="" />
</div>

{{% /grid %}}
{{% grid class="col-md-6" %}}

Sense3 mime les bannières utilisés par les régies publicitaires et les
codes marketing de leurs annonceurs dans l’objectif de promouvoir
les projets alternatifs qui œuvrent pour l’intérêt général.

{{% grid class="text-center" %}}
<script src="/s/sense3.js" data-sense3="300x250;art"></script>
{{% /grid %}}

Ces projets sont :

1. Libres (= open source + éthiques)
2. Non lucratifs
3. Respectueux de votre vie privée

Évidemment Sense3 respecte ces mêmes critères.

{{% /grid %}}<p>
{{% /grid %}}
{{% grid class="col-sm-12 text-center" %}}

[Pour commencer](#getting-started) [Comment ça fonctionne](#how-it-works)

{{% /grid %}}<p>
{{% /grid %}}<p>
{{% /grid %}}
{{% grid class="row how-it-works" %}}
{{% grid class="container" %}}
{{% grid class="col-md-8" %}}

### Respectueux de votre vie privée {#how-it-works}

Sense3 ne vous piste pas.
Les bannières affichées sont uniquement « ciblées »
en fonction des informations données « naturellement » par
le navigateur web (pour une navigation mieux adaptée à vos usages).
Ces informations déterminent quels liens seront affichés.

Par exemple, grâce à l’[User Agent](https://fr.wikipedia.org/wiki/User_agent),
si vos visiteur·euse·s utilisent le navigateur Google Chrome, Sense3 affichera une bannière
vous invitant à essayer la dernière version de [Firefox](https://www.mozilla.org/firefox/).

De même, avec le [Référent](https://fr.wikipedia.org/wiki/R%C3%A9f%C3%A9rent_(informatique)),
il est possible de connaître la page consultée précédemment.
Si les visiteur·euse·s viennent depuis Twitter, un encart invitant à rejoindre
[le réseau social Mastodon](https://joinmastodon.org) s’affichera probablement dans la page.

Aucune information personnelle ou de statistique n’est collectée.

{{% /grid %}}
{{% grid class="col-md-4 text-center" %}}

<script src="/s/sense3.js" data-sense3="300x250;;;showInfos"></script>

{{% /grid %}}
{{% grid class="col-md-8 col-md-push-4" %}}

### Non lucratif

Notre but est de prendre soin de notre patrimoine naturel numérique commun.

Internet étant devenu un espace de plus en plus marchand, il devient
difficile de trouver des espaces créatifs où l’on ne vous tient pas responsables
de la destruction d’emplois parce que vous utilisez un bloqueur de pub.

Ici, la rémunération des « annonceurs » est calculée en fonction du nombre de clics (CPC) et de vues (CPV).
Les paiements s’effectuent chaque semaine sous forme de gratitude et de reconnaissance de la part
des webmasters qui font le choix d’afficher ces bannières.

La liste des bannières est également définie avec beaucoup d’amour et de partialité.

{{% /grid %}}
{{% grid class="col-md-4 col-md-pull-8" %}}

<script src="/s/sense3.js" data-sense3="300x250;;;sense3"></script>

{{% /grid %}}
{{% grid class="col-md-8" %}}

### Libre

Le logiciel est libre. Il est sous [licence MIT](https://framagit.org/josephk/sense3/blob/master/LICENSE),
vous pouvez l’installer pour votre propre usage, le modifier,
[proposer des améliorations ou de nouvelles bannières](https://framagit.org/josephk/sense3)…

Son développement est assuré par [JosephK](https://liberapay.com/josephk/),
employé de l’association [Framasoft](https://framasoft.org).

Le projet est né d’une idée de [poisson d’Avril pour le Framablog](https://framablog.org/2018/04/01/framadsense-la-publicite-qui-a-du-sens).

{{% /grid %}}
{{% grid class="col-md-4 text-center" %}}

<script src="/s/sense3.js" data-sense3="300x250;;;sense3Fork"></script>

{{% /grid %}}
{{% grid class="col-sm-12" %}}

[Pour commencer](#getting-started)

{{% /grid %}}
{{% /grid %}}
{{% /grid %}}
{{% grid class="row getting-started" %}}
{{% grid class="container" %}}
{{% grid class="col-sm-12" %}}

## Ajouter une bannière sur votre site {#getting-started}

<script src="/s/sense3.js" data-sense3-generator="true"></script>

{{% /grid %}}
{{% /grid %}}
{{% /grid %}}
<p>

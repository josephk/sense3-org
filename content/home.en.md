+++

date = "2018-02-28T14:41:00+01:00"
title = "Home"
author = "JosephK"
draft = false
type = "page"
id = "-home"
+++

{{% grid class="row intro" %}}
{{% grid class="container" %}}
{{% grid class="clearfix" %}}
{{% grid class="col-sm-12 text-center" %}}

# “This is not an ad”

## Let's advertise for projects that make sense

{{% /grid %}}
{{% /grid %}}
{{% grid class="clearfix" %}}
{{% grid class="col-md-6 text-center" %}}
<div class="web-browser">
    <div class="toolbar">
        <img src="/browser-left.svg" alt="" />
        <div class="search-bar"></div>
        <img src="/browser-right.svg" alt="" />
    </div>
    <img src="/wireframe.png" class="img-responsive" alt="" />
</div>

{{% /grid %}}
{{% grid class="col-md-6" %}}

Sense3 mimics banners used by advertising agencies and
marketing codes of their advertisers in order to promote
alternative projects working for the general interest.

{{% grid class="text-center" %}}
<script src="/s/sense3.js" data-sense3="300x250;art"></script>
{{% /grid %}}

These projects are:

1. Libre (= open source + ethics)
2. Non lucrative
3. Respecting your privacy

Obviously Sense3 meets these same criteria.

{{% /grid %}}<p>
{{% /grid %}}
{{% grid class="col-sm-12 text-center" %}}

[Get started](#getting-started) [How it works](#how-it-works)

{{% /grid %}}<p>
{{% /grid %}}<p>
{{% /grid %}}
{{% grid class="row how-it-works" %}}
{{% grid class="container" %}}
{{% grid class="col-md-8" %}}

### Respecting your privacy {#how-it-works}

Sense3 is not tracking you.
The displayed banners are only "targeted".
according to the information given "naturally" by
the web browser (for a navigation better adapted to your uses).
This information determines which links will be displayed.

For example, through the [User Agent](https://en.wikipedia.org/wiki/User_agent),
if your visitors use the Google Chrome browser, Sense3 will display a banner
inviting you to try the latest version of [Firefox](https://www.mozilla.org/firefox/).

Similarly, with the [Referer](https://en.wikipedia.org/wiki/HTTP_referer),
it is possible to know the page previously consulted.
If visitors come from Twitter, an insert inviting them to join
[the Mastodon social network](https://joinmastodon.org) will probably appear on the page.

No personal or statistical information is collected.

{{% /grid %}}
{{% grid class="col-md-4 text-center" %}}

<script src="/s/sense3.js" data-sense3="300x250;;;showInfos"></script>

{{% /grid %}}
{{% grid class="col-md-8 col-md-push-4" %}}

### Non lucrative

Our goal is to take care of our common digital natural heritage.

As the Internet has become more and more a marketplace, it is becoming
more difficult to find creative spaces where you are not held accountable
from destroying jobs because you're using an ad blocker.

Here, the remuneration of "advertisers" is calculated according to the number of clicks (CPC) and views (CPV).
Payments are made weekly in the form of gratitude and recognition from
webmasters who choose to display these banners.

The list of banners is also defined with a lot of love and bias.

{{% /grid %}}
{{% grid class="col-md-4 col-md-pull-8" %}}

<script src="/s/sense3.js" data-sense3="300x250;;;sense3"></script>

{{% /grid %}}
{{% grid class="col-md-8" %}}

### Libre

The software is free. It is under [MIT license](https://framagit.org/josephk/sense3/blob/master/LICENSE),
you can install it for your own use, modify it,
[proposing improvements or new banners](https://framagit.org/josephk/sense3)…

Its development is ensured by [JosephK](https://liberapay.com/josephk/),
an employee of the association [Framasoft](https://framasoft.org).

The project was born from an [April fools idea for the Framablog](https://framablog.org/2018/04/01/framadsense-la-publicite-qui-a-du-sens).

{{% /grid %}}
{{% grid class="col-md-4 text-center" %}}

<script src="/s/sense3.js" data-sense3="300x250;;;sense3Fork"></script>

{{% /grid %}}
{{% grid class="col-sm-12" %}}

[Get started](#getting-started)

{{% /grid %}}
{{% /grid %}}
{{% /grid %}}
{{% grid class="row getting-started" %}}
{{% grid class="container" %}}
{{% grid class="col-sm-12" %}}

## Add a banner on your site {#getting-started}

<script src="/s/sense3.js" data-sense3-generator="true"></script>

{{% /grid %}}
{{% /grid %}}
{{% /grid %}}
<p>


